#!/usr/bin/env python3
# -*- coding: utf-8 -*- #

from urllib.parse import urljoin
from os import environ
from os.path import join as path_join

AUTHOR = 'Lukas Pirl'
SITENAME = 'Lukas Pirl'
SITEURL = ''

PATH = 'content'
PAGE_PATHS = ['']
ARTICLE_PATHS = []

IGNORE_FILES = [
  '.#*',
  '*.part.*',
  'README*',
]

THEME_TEMPLATES_OVERRIDES = [
  'templates',
]

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

SOCIAL = tuple()

DEFAULT_PAGINATION = False

RELATIVE_URLS = False

STATIC_PATHS = [
  'static',
  'robots.txt',
  '_redirects',
]

STATIC_CREATE_LINKS = True

FILENAME_METADATA = r'(?P<title>(?P<date>\d{4}-\d{2}-\d{2})-.*)'

# We extract the full path; and the path without the file extension or
# trailing ``index.html`` (also used as slug) here. E.g.:
# * for ``…/foo/bar/index.rst``
#   * path w/o extension: ``…/foo/bar/index``
#   * URL path / slug: ``…/foo/bar``
# * for ``…/foo/bar.rst``
#   * path w/o extension: ``…/foo/bar``
#   * URL path / slug: ``…/foo/bar``
# We use those two named groups to construct the ``*_URL`` and
# ``*_SAVE_AS`` variables.
PATH_METADATA = r'(?P<path_no_ext>(?P<slug>.*?)(/?index)?)\..*'
# Additionally, we match the first path component for pages
# (in order to to strip it):
PATH_METADATA = f'(({"|".join(PAGE_PATHS)})/)?' + PATH_METADATA

PAGE_URL = PAGE_LANG_URL = ARTICLE_URL = ARTICLE_LANG_URL = '{slug}'
PAGE_SAVE_AS = PAGE_LANG_SAVE_AS = ARTICLE_SAVE_AS = \
  ARTICLE_LANG_SAVE_AS = '{path_no_ext}.html'

# for article/post:
USE_FOLDER_AS_CATEGORY = False
DEFAULT_CATEGORY = 'general'

THEME = 'themes/m.css/pelican-theme'
THEME_STATIC_DIR = 'static'

M_THEME_COLOR = '#000000'

########################################################################
#
# plugins
#
PLUGIN_PATHS = []
PLUGINS = []

PLUGIN_PATHS.append('themes/m.css/plugins')
PLUGINS.append('m.htmlsanity')
PLUGINS.append('m.components')

M_BLOG_URL = '/posts'

M_CSS_FILES = [
  'static/lukas-pirl.css',
]

M_FAVICON = (
  '/static/favicon.svg',
  'image/x-icon'
)

M_SITE_LOGO = None

"""
A list of four-tuples:
  * link title
  * URL
  * slug (used to highlight currently active menu item)
  * sub-menu items, 3 tuples:

    * link title
    * URL
    * page slug

On narrow screens, the navbar is divided into two columns, links from
the first variable are in the left column while links from the second
variable are in the right column.

See also: https://mcss.mosra.cz/themes/pelican/#top-navbar
"""
M_LINKS_NAVBAR1 = [
  ('IT', 'IT', None, []),
  ('audio', 'audio', None, []),
  ('misc', 'miscellaneous', None, []),
  ('contact', 'contact', None, []),
]

# a menu in the footer; just like M_LINKS_NAVBAR{1…4}:
M_LINKS_FOOTER1 = [
  ('site sources', 'https://gitlab.com/lpirl/lukas-pirl.de'),
]
M_LINKS_FOOTER2 = [
  ('Impressum (GER)', 'Impressum'),
]
M_LINKS_FOOTER3 = [
  ('legal notice', 'legal-notice'),
]
M_LINKS_FOOTER4 = [
  ('privacy policy', 'privacy-policy/gitlab'),
]

# text in footer:
M_FINE_PRINT = None

M_COLLAPSE_FIRST_ARTICLE = True

PLUGINS.append('sitemap')
SITEMAP = {
  'format': 'xml',
  'priorities': {
    'articles': 0.1,
    'indexes': 0.6,
    'pages': 0.5,
  },
  'changefreqs': {
    'articles': 'yearly',
    'indexes': 'monthly',
    'pages': 'monthly',
  }
}

PLUGINS.append('gitlab_projects')
GITLAB_USER = 'lpirl'
GITLAB_CONTRIBUTED_PROJECTS = True
