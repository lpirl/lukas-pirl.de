MAKEFILE = $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
BASEDIR = $(shell dirname '$(MAKEFILE)')

PELICAN ?= pelican
PELICANOPTS ?=

INPUTDIR ?= $(BASEDIR)/content
CONFFILE ?= $(BASEDIR)/pelicanconf.py
OUTPUTDIR ?= $(BASEDIR)/public
PUBLISHCONF ?= $(BASEDIR)/publishconf.py

FONTURL ?= 'https://gwfh.mranftl.com/api/fonts/courier-prime?download=zip&subsets=latin&variants=regular&formats=woff2,woff,ttf,svg'
FONTZIP := $(shell mktemp)
FONTDIR ?= $(INPUTDIR)/static/fonts

LINKCHECK_SERVER_PIDFILE = $(BASEDIR)/linkcheck-server.pid

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

ifeq (,$(findstring .local/bin,$(PATH)))
	export PATH:=$(HOME)/.local/bin:$(PATH)
endif

PIPOPTS ?= --upgrade --upgrade-strategy=eager
ifeq ($(VIRTUAL_ENV),)
  PIPOPTS += --user
endif

define rkill_shell_func
	rkill () { \
		local children=$$(ps -o pid= --ppid "$$1"); \
		for pid in $$children; do rkill "$$pid"; done; \
		echo $$children | xargs -r kill; \
	}
endef

help:
	@pygmentize -gO style=fruity $(MAKEFILE) || cat $(MAKEFILE)
	@echo --
	@echo :p

$(OUTPUTDIR):
	mkdir $@
	-chattr +C $@

$(FONTDIR):
	wget -O $(FONTZIP) $(FONTURL)
	mkdir $@
	cd $@ && unzip $(FONTZIP)
	rm $(FONTZIP)

html: $(OUTPUTDIR) $(FONTDIR)
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

publish: CONFFILE=$(PUBLISHCONF)
publish: html

regenerate: override PELICANOPTS += -r
regenerate: html

devserver: PORT ?= 8000
devserver: override PELICANOPTS += -lp $(PORT)
devserver: regenerate

setup:
	# preserve installation order of requirements as specified in the
	# requirements file (since when does pip mess with it?!)
	egrep -v '^\s*#' pip-requirements.txt \
	| xargs -rtL1 pip3 install $(PIPOPTS)

pages: PELICANOPTS += --fatal=warnings
pages: publish precompress

precompress:
	find $(OUTPUTDIR) -type f -not -name '*.gz' -printf '%p.gz\0' \
	| xargs -0r $(MAKE) -j $$(nproc)

%.gz: %
	gzip --keep --best --force --no-name '$<'

check-files-attached:
	(cd $(INPUTDIR) && find -type f ) \
	| egrep -v '/.git(ignore|attributes)?$$' \
	| egrep -v '\.(html?|rst)$$' \
	| egrep -v '([^/]+\.)?README(\.[^/]+)?$$' \
	| sed 's|^\./pages/|./|' \
	| sed 's|^\.|$(OUTPUTDIR)|' \
	| tr "\n" "\0" \
	| xargs -0 ls -- 2>&1 1>/dev/null \
	| sed 's|^ls: cannot access |expected but absent: |' \
	| grep .; \
	test $$? -eq 1

check-links: PORT = 8123
check-links: PELICANOPTS += --quiet
check-links: OUTPUTDIR = linkchecker-htdocs
check-links:

	# ensure clean built is ready before we start the devserver
	# (because link check will start right away):
	$(MAKE) OUTPUTDIR=$(OUTPUTDIR) clean
	$(MAKE) OUTPUTDIR=$(OUTPUTDIR) html

	# start a devserver in the background
	( \
		$(MAKE) PORT=$(PORT) devserver & \
		echo $$! > $(LINKCHECK_SERVER_PIDFILE) \
	)

	@# readability fixes welcome :D
	# start a subshell for link checking which traps exits to terminate
	# the devserver
	bash -c ' \
		$(rkill_shell_func); \
		trap "rkill \$$(cat $(LINKCHECK_SERVER_PIDFILE))" EXIT; \
		until curl -s http://localhost:$(PORT) -o /dev/null; \
			do sleep .5; echo waiting for devserver to be ready…; done; \
		linkchecker -f .linkcheckerrc http://localhost:$(PORT) || true; \
	'

	exit $$(grep -c 'class="error"' linkchecker-out.html)

clean:
	find "$(BASEDIR)" \
		-not -path "$(INPUTDIR)/*" \
		-not -path "$(OUTPUTDIR)/*" \
		-name '*.pyc' \
		-or \
		-name '*.pyo' \
		-print \
		-delete
	rm -rf \
		$(OUTPUTDIR)

distclean: clean
	rm -rf \
		$(FONTDIR) \
		__pycache__
