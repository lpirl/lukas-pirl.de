==============
privacy policy
==============

:description:
  privacy policy for web services offered by Lukas Pirl, hosted on
  GitLab pages



This website is hosted on `GitLab.com <https://about.gitlab.com/>`__.
Please refer to the information about the
`privacy policy <https://about.gitlab.com/privacy/>`__ and the
`privacy compliance <https://about.gitlab.com/privacy/privacy-compliance/>`__
provided by GitLab Inc. accordingly.

For services offered on subdomains of this domain and services offered
on other domains that are operated by the same domain owner, please
refer to the `general privacy policy <{filename}index.rst>`__.
