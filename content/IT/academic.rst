===================
academic activities
===================

:breadcrumb:
  {filename}/IT/index.rst IT

:description:
  list of academic activities by Lukas Pirl

:template: redirect
:redirect_url: https://osm.hpi.de/people/pirl/

.. This page/URL can be used on platforms to link to academic activities
.. so there is a central point in our control (this repository) where
.. the linking to the actual content (currently on osm.hpi.de) happens.
