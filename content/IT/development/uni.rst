============
uni projects
============

:breadcrumb:
  {filename}/IT/index.rst IT
  {filename}/IT/development/index.rst development

:description:
  list of long-ago uni projects by Lukas Pirl



For current academic activities, please refer to the
`academic profile <https://osm.hpi.de/people/pirl>`__.

This page lists past projects from B.Sc. and M.Sc. studies.


master's project on instantlab.org
==================================

public platform to run operating system experiments in the Cloud

main contributions: software architecture, data modeling;
`multi-processing-compatible and asynchronous DNAT port forwarding
<https://github.com/lpirl/pynat>`__, OpenID authentication,
Scrum master

2014



bachelor's project: an IT system for the Berlin water rescue service
====================================================================

main contributions: development of access-controlled and
traceable object versioning in an object-relational mapper;
data modeling; requirements engineering

`website
<https://hpi.de/en/giese/teaching/bachelor-projects/von-der-spezifikation-zum-strandeinsatz.html>`__ (German language only)

2011
