===============
web development
===============

:breadcrumb:
  {filename}/IT/index.rst IT
  {filename}/IT/development/index.rst development

:description:
  list of activities by Lukas Pirl related to web development



email list service `eemaill.org <http://eemaill.org/>`__
========================================================

design, development, operation

since 2010



email list service `lists.myhpi.de <https://lists.myhpi.de>`__
==============================================================


a clone of `eemaill.org <http://eemaill.org/>`__ for HPI students

design, development, operation

2012 – 2021



`hotel broker Berlin – Brandenburg <https://hotelbroker-bb.de/>`__
==================================================================

consulting, support

since 2021



`hotel consulting Berlin – Brandenburg <https://hotelconsulting-bb.de/>`__
==========================================================================

consulting, support

since 2019



`Convention Hotel & Touristic Service Berlin – Brandenburg <https://convention-bb.de/>`__
=========================================================================================

consulting, support

since 2018



Tonmeisterin `Anne Taegert <https://anne-taegert.de>`__
=======================================================

consulting, development, support

2017



band `Operation Zeit <http://operation-zeit.de/>`__
===================================================

design, development, operation

since 2011



band `Antiqua Potsdam <http://www.antiqua-potsdam.com/>`__
==========================================================

consulting, customization, maintenance

2014 – 2018



architect `Katrin Seeger <https://web.archive.org/web/202205/https://katrin-seeger-architektin.de/>`__
======================================================================================================

consulting, design, development, maintenance

2010 – 2017



tax consultant `Yvonne Leiter <http://www.steuerberatung-leiter.de/>`__
=======================================================================

consulting, design, development, maintenance

2007 – 2017



agency for qualified employees "accedera"
=========================================

consulting, customization

2013 – 2015



band "Psychotrie"
=================

design, development, operation

2008 – 2011
