==========================
other development profiles
==========================

:breadcrumb:
  {filename}/IT/index.rst IT
  {filename}/IT/development/index.rst development

:description:
  list of not actively used development profiles of Lukas Pirl



Actively used profiles regarding development are listed on the
`"development" page <{filename}index.rst>`__.

.. container:: m-text m-dim

  Although merely used, please find a list of profiles on other
  development platforms below.

  - `Gnome GitLab <https://gitlab.gnome.org/lpirl>`__
  - `SourceForge <https://sourceforge.net/users/lpirl>`__
  - `BitBucket <https://bitbucket.org/lpirl>`__
  - `Gravatar <https://en.gravatar.com/lpirl>`__
