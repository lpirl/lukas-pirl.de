======
GitLab
======

:template: gitlab-projects

:breadcrumb:
  {filename}/IT/index.rst IT
  {filename}/IT/development/index.rst development

:description:
  list of contributions at GitLab.com by Lukas Pirl



Please see GitLab.com for lists of

- `personal projects <https://gitlab.com/users/lpirl/projects>`__,
- `projects contributed to <https://gitlab.com/users/lpirl/contributed>`__,
- and `snippets <https://gitlab.com/users/lpirl/snippets>`__.
