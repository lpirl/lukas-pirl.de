===========
development
===========

:breadcrumb:
  {filename}/IT/index.rst IT

:html_header:
  <style>
  div.m-dim ul > li { float: left; margin-right: 3em; }
  #not-actively-used-profiles { color: var(--dim-color); }
  #not-actively-used-profiles a { color: var(--dim-color); }
  </style>

:description:
  list of activities by Lukas Pirl related to software development



most of the current public development activities can be found on:
==================================================================

`GitLab profile <https://gitlab.com/lpirl>`__
---------------------------------------------

`GitHub profile <https://github.com/lpirl>`__
---------------------------------------------



further activities
==================

`web development <{filename}web.rst>`__
---------------------------------------

`uni projects <{filename}uni.rst>`__
------------------------------------
