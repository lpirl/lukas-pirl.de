=====================
IT-related activities
=====================

:description:
  list of activities by Lukas Pirl related to IT



`academia <https://osm.hpi.de/people/pirl>`__
=============================================

`development <{filename}development/index.rst>`__
=================================================

`operation <{filename}operation.rst>`__
=======================================
