============
IT operation
============

:breadcrumb:
  {filename}/IT/index.rst IT

:description:
  list of activities by Lukas Pirl related to operation of IT



`soli.tools <https://soli.tools>`__
===================================

idea, setup, maintenance

since 2024



`Armbian community maintainer <https://www.armbian.com/nanopineo3>`__ for `NanoPi NEO3 <https://wiki.friendlyelec.com/wiki/index.php/NanoPi_NEO3>`__
=====================================================================================================================================================

since 2022



`assistance for the infrastructure <https://osm.hpi.de/people/pirl/#infrastructure>`__ of the `Professorship for Operating Systems and Middleware <https://osm.hpi.de/>`__
==========================================================================================================================================================================

|HPI|, |UP|

since 2017



servers of the `system analysis and modelling group <https://hpi.de/en/giese/home.html>`__
==========================================================================================

|HPI|, |UP|

main tasks: consultation, migration, maintenance, disaster standby

2014 – 2018



server for students
===================

|HPI|, |UP|

shell access, Web hosting (multiple languages),
databases (multiple RDBMS) for ~900 students

FreeBSD, integrated in the institute's corporate IT landscape
(NIS, Kerberos, etc.)

main tasks: planning and setup of new server hardware, migration to
the new server, maintenance, ongoing improvement and extension of
the services provided

2010 – 2013



`server of the student representatives <http://myhpi.de/>`__
============================================================

|HPI|, |UP|

2010 – 2013



e-learning platform
===================

`Didactics of Physics
<https://www.uni-potsdam.de/de/physikdidaktik/willkommen>`__,
|UP|

consulting, setup

2011

and of course, some of the `websites <{filename}development/web.rst>`__
=======================================================================

.. .....................................................................

.. replacements:

.. |OS, GI| replace::
  `Operating Systems working group <https://www.betriebssysteme.org/>`__
  of the `German Informatics Society <https://gi.de/en/>`__
  (`Gesellschaft für Informatik <https://gi.de/>`__)

.. |HPI| replace::
  `Hasso Plattner Institute <https://hpi.de>`__

.. |UP| replace::
  `University of Potsdam <https://uni-potsdam.de>`__
