=====
audio
=====

:description:
  list of activities by Lukas Pirl related to audio



`live sound <{filename}live-sound.rst>`__
=========================================



`Kosmoskonzerte <https://rz-potsdam.de/soziokreatives-rzentrum/projekte/kosmoskonzerte/>`__
===========================================================================================

A monthly concert series, providing a cozy, intimate, and concentrated
atmosphere for indie, neoclassical, singer-songwriter, and alike acts.
Run by a collective of volunteers.

main activities: planning and doing `live sound
<{filename}live-sound.rst>`__, general organization of the
series, and management of technical equipment

2017 – 2024



`Operation Zeit <http://operation-zeit.de/>`__
==============================================

Rap with band – crossover hip hop from acoustic and electronic
instruments.

drums, recording, post-production

since 2010



`Musikwerkstatt <https://kuze-potsdam.de/#projekte>`__
======================================================

A rehearsal room collective.

main activities: maintenance of sound reinforcement equipment and room
acoustics,  treasurer, general organization (and rehearsing, of course)

2007 – 2023



`DIY: acoustic pictures <{filename}DIY-acoustic-pictures/index.rst>`__
=======================================================================



`DIY: flexible acoustic treatment <{filename}DIY-acoustic-treatment/index.rst>`__
=================================================================================



`freesound <http://freesound.org/people/lukas_p/>`__
====================================================

contribution of samples
