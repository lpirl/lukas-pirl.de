===========================================================
live sound
===========================================================

:breadcrumb:
  {filename}/audio/index.rst audio

:description:
  list of activities by Lukas Pirl related to live sound



.. BEGIN shortcuts

.. |rz| replace:: `Rechenzentrum Potsdam <https://rz-potsdam.de>`__
.. |kuze| replace:: `KuZe Potsdam <https://kuze-potsdam.de>`__
.. |ceeys| replace:: `CEEYS <https://web.archive.org/web/2020/https://ceeys.de>`__
.. |bs| replace:: `Brueder Selke <https://www.bruederselke.com>`__
.. |fL| replace:: `freiLand Potsdam <https://freiland-potsdam.de>`__
.. |haus2| replace:: `hausZwei
                     <https://freiland-potsdam.de/hauszwei/aboutkontakt>`__
.. END shortcuts

.. BEGIN approximate format
..
..   headline
..   =========================================================
..
..   <note (if required)>
..
..   <act (if not in headline)>
..
..   <role/activity>
..
..   <date>, <place>
..
.. END approximate format



`Kosmoskonzerte #80 <https://rz-potsdam.de/termin/kosmoskonzerte-80>`__
=======================================================================

`Bernadette La Hengst <http://www.lahengst.com/>`__,
`Grateful Cat <https://gratefulcat.bandcamp.com/>`__

planning, setup, mixing

2024-12-02, |rz|



`Kosmoskonzerte #79 <https://rz-potsdam.de/termin/kosmoskonzerte-79>`__
=======================================================================

`Chris Imler <http://www.chrisimler.com>`__,
`BUHAI <http://www.buhai-band.de>`__

planning, setup, mixing

2024-11-25, |rz|



`Kosmoskonzerte #78 <https://rz-potsdam.de/termin/kosmoskonzerte-78>`__
=======================================================================

`Henny Herz <https://www.hennyherz.com>`__,
`Maria De Val <https://mariadeval.bandcamp.com>`__

planning, setup, mixing

2024-10-07, |rz|



`Kosmoskonzerte #77 <https://rz-potsdam.de/termin/kk-77>`__
============================================================

`What Are People For? <http://whatarepeoplefor.bandcamp.com>`__,
`Manu Louis <http://manulouis.bandcamp.com>`__

planning, setup, mixing

2024-09-01, |rz|



`concert with BANTU <https://rz-potsdam.de/termin/bantu>`__
===========================================================

`BANTU <https://www.bantucrew.com>`__

in the team for planning, setup, mixing

2024-07-12, |rz|



`Kosmoskonzerte #76 <https://rz-potsdam.de/termin/kosmoskonzerte-76>`__
=======================================================================

`Lunya <https://www.facebook.com/Lunya-208703526706157>`__,
`Korridor <https://www.instagram.com/korridorofficial>`__

planning, setup, mixing

2024-07-08, |rz|



`Kosmoskonzerte #75 <https://rz-potsdam.de/termin/kosmoskonzerte-75>`__
=======================================================================

`Meret Ester <https://www.meretester.org>`__,
`Kino Motel <https://www.kino-motel.com>`__

planning, setup, mixing

2024-06-03, |rz|


`Kosmoskonzerte #74 <https://rz-potsdam.de/termin/kosmoskonzerte-74>`__
=======================================================================

Elinea,
`The Keeds <https://www.the-keeds.de>`__

planning, setup, mixing

2024-05-05, |rz|



`Kosmoskonzerte #73 <https://rz-potsdam.de/termin/kosmoskonzerte-73>`__
=======================================================================

`Gigoglo Tears <https://gigolotears.bandcamp.com>`__,
`Juno Lee <https://www.junolee.band>`__

planning, setup, mixing

2024-04-08, |rz|



`Kosmoskonzerte #72 <https://rz-potsdam.de/termin/kosmoskonzerte-72>`__
=======================================================================

`Nina Bauer <https://ninabauer.bandcamp.com>`__,
`Lightcap <https://lightcapmusic.com>`__

in the team for booking, planning; setup, mixing

2024-03-04, |rz|



`Kosmoskonzerte #71 <https://rz-potsdam.de/termin/kosmoskonzerte-71>`__
=======================================================================

`Vati <https://bittevati.bandcamp.com>`__,
`Joanna Waluszko <http://www.waluszko.eu>`__

planning, setup, mixing

2024-02-05, |rz|


`Chanukah I – Ein Prologue <https://freiland-potsdam.de/25028>`__
=================================================================

`Thorsten Müller <https://thorstenmüller.de>`__ (clarinet),
`Eleonora Savini <https://www.eleonorasavini.com>`__ (violin),
|bs| (cello, piano),
Kate Hannah Weinrieb & Sebastian Weise (script, reading)

planning, setup, mixing

2023-12-07, 2023-12-08, |haus2|



`Kosmoskonzerte #70 <https://rz-potsdam.de/termin/kosmoskonzerte-70>`__
=======================================================================

`Paula Paula <https://www.paulapaulamusik.de>`__,
`Paul Sies <https://paulsies.com>`__

planning, setup, mixing

2023-12-04, |rz|



`Kosmoskonzerte #69 <https://rz-potsdam.de/termin/kosmoskonzerte-69>`__
=======================================================================

`Twin Tooth <https://linktr.ee/twintooth>`__,
`Oh No Noh <http://oh-no-noh.de>`__

planning, setup, mixing

2023-11-06, |rz|



`Kosmoskonzerte #68 <https://rz-potsdam.de/termin/kosmoskonzerte-68>`__
=======================================================================

`Vale <https://linktr.ee/valenirvana>`__,
`Laure Boer <https://laureboer.com>`__,
|bs|

in the team for planning, setup, mixing

2023-10-09, |rz|



`Kosmoskonzerte #67 <https://rz-potsdam.de/termin/kosmoskonzerte-67>`__
=======================================================================

`Dino Paris
<https://kreismusik.de/de/dino-paris-und-der-chor-der-finsternis/>`__,
`Kaskadeur <https://kaskadeur.net/>`__

planning, setup, mixing

2023-09-02, |rz|



`Kosmoskonzerte #66 <https://rz-potsdam.de/termin/kosmoskonzerte-66>`__
=======================================================================

`Jens Friebe <http://www.jens-friebe.de>`__,
`Cremant Ding Dong <https://cremantdingdong.bandcamp.com>`__

planning, setup, mixing

2023-08-07, |rz|



`Prærie Festival <https://praerie-festival.com>`__
==================================================

in the team of backliners and sound runners

2023-07-27 – 2023-07-31, Casel/Kózle, Drebkau



`Kosmoskonzerte #65 <https://rz-potsdam.de/termin/kosmoskonzerte-65>`__
=======================================================================

`MPercussion <https://www.facebook.com/MPercussion/>`__
& `Emse Patu <https://www.instagram.com/emsepatu/>`__,
|bs|

planning, setup, mixing

2023-07-03, |rz|



`Kosmoskonzerte #64 <https://rz-potsdam.de/termin/kosmoskonzerte-64>`__
=======================================================================

`Naicke <https://naicke.net>`__ & Nawir,
`Malonda <https://malondamusik.bandcamp.com>`__

planning, setup, mixing

2023-06-05, |rz|



`Q3Ambientfest <https://www.bruederselke.com/aktuell/q3ambientfest-ransom-note-1>`__
====================================================================================

`Simon Ansing <https://soundcloud.com/simonansing>`__,
`Sergio Díaz De Rojas <https://bonjoursergio.store>`__,
`Yair Elazar Glotmann <https://yairelazarglotman.com>`__,
`Jakob Lindhagen <https://jakoblindhagen.com>`__,
`Tim Linghaus <https://www.timlinghaus.com>`__,
`Johannes Malfatti <https://www.johannesmalfatti.com>`__,
`Ben Osborn <http://ben-osborn.com>`__,
`Sofi Paez <https://sofipaez.bandcamp.com>`__,
`Julia Reidy <http://julia-reidy.com>`__,
`Grand River <https://grandriver.eu>`__,
|bs|,
`Alex Stolze <http://alexstolze.com>`__,
`Cedric Vermue <https://myljarecords.bandcamp.com>`__,
`Vargkvint <https://vargkvint.bandcamp.com>`__,
`Villemin <https://www.villeminmusic.com>`__,
Video Acts

in the team for planning, setup, mixing

2023-06-02 – 2023-06-04, `Waschhaus Potsdam <https://www.waschhaus.de>`__



`Kosmoskonzerte #63 <https://rz-potsdam.de/termin/kosmoskonzerte-63>`__
=======================================================================

`Nifty5 <https://nifty5.bandcamp.com>`__,
`Angela Aux <https://www.angela-aux.com>`__

planning, setup, mixing

2023-05-07, |rz|



`Kosmoskonzerte #62 <https://rz-potsdam.de/termin/kosmoskonzerte-62>`__
=======================================================================

Benja Schlez,
`Eric Maltz <https://ericmaltz.com>`__,
|bs|

assistance

2023-04-17, |rz|



`Kosmoskonzerte #61 <https://rz-potsdam.de/termin/kosmoskonzerte-61>`__
=======================================================================

`no:la <https://no-la.band>`__,
`Can`t spell KWU <https://www.instagram.com/cantspellkwu>`__

planning, setup, mixing

2023-03-27, |rz|



`Kosmoskonzerte #60 <https://rz-potsdam.de/termin/kosmoskonzerte-60>`__
=======================================================================

`NICHTSEATTLE <https://linktr.ee/nichtseattle>`__,
`Petula <https://petula.org>`__

planning, setup, mixing

2023-02-20, |rz|



`Kosmoskonzerte #59 <https://rz-potsdam.de/termin/kosmoskonzerte-59>`__
=======================================================================

`Enzo Caterino <https://www.facebook.com/enzocaterino.cello>`__,
`Iván Muela <https://www.ivanmuela.com>`__,
|bs|

planning, setup, mixing

2023-01-09, |rz|



`Kosmoskonzerte #58 <https://rz-potsdam.de/termin/kala-brisella-shitney-beers/>`__
==================================================================================

`Kala Brisella <https://kalabrisella.bandcamp.com>`__,
`Shitney Beers <https://shitneybeers.bandcamp.com>`__

planning, setup, mixing

2022-12-02, |rz|



`Kosmoskonzerte #57 <https://rz-potsdam.de/termin/kosmoskonzerte-57/>`__
========================================================================

`Henny Herz & Band <https://www.hennyherz.com>`__,
`Rebekka & Juki <https://rebekkaundjuki.de>`__

planning, setup, mixing

2022-11-21, |rz|



`Kosmoskonzerte #56 <https://rz-potsdam.de/cms/event/kosmoskonzerte-56>`__
==========================================================================

`Joanna Gemma Auguri <https://joannagemmaauguri.com>`__,
`Ben Osborn <http://ben-osborn.com>`__,
|bs|

planning, setup, mixing

2022-10-10, |rz|



`Traumtext 2022 <https://freiland-potsdam.de/20475>`__
======================================================

sound and performance, movie screening

`Alex Nowitz <https://nowitz.de>`__ (voice),
`Jürgen Kupke <https://www.juergenkupke.de>`__ (clarinet),
`Nicolas Schulze <http://www.nicolasschulze.de>`__ (Fender Rhodes),
`Thorsten Müller <https://thorstenmüller.de>`__ (bass clarinet & concept)

planning, setup, mixing

2022-09-17, |haus2|, |fL|



Kosmoskonzerte #55
==================

`INGA <https://trikont.de/category/artists/inga/>`__,
`Neoangin (Jim Avignon)  <http://www.jimavignon.com>`__

planning, setup, mixing

2022-09-02, |rz|



`Hypergraphia <https://hypergraphia.de>`__ festival
====================================================

`BOYZWHOCRY <https://linktr.ee/boyzwhocry>`__,
`Fallwander <http://www.zaremba-music.com/fallwander>`__,
`Dino Paris
<https://kreismusik.de/de/dino-paris-und-der-chor-der-finsternis/>`__

planning, mixing

2022-08-13, |fL|



`Trokut <https://www.trokutband.com>`__
========================================

planning

2022-07-14, |kuze|



Kosmoskonzerte #53
==================

`5 String Theory <https://www.alexeyviolin.com/copy-of-5-string-theory>`__,
|bs|

planning, setup, mixing

2022-07-04, |rz|



Fête de la Musique
==================

`can’t spell KWU <https://www.instagram.com/cantspellkwu>`__,
`Micha Palimm <https://soundcloud.com/dukat_palimm>`__,
`Yeon <https://yeonband.wordpress.com>`__

planning, in the team for setup and mixing

2022-06-21, |rz|



Kosmoskonzerte #52
==================

`Knixx <https://knixx.bandcamp.com>`__,
`Yetundey <https://yetundey.com>`__

in the team for booking

2022-06-13, |rz|



`Kosmoskonzerte #51 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte51-mit-baldabiou-und-club-laguna/>`__
=============================================================================================================================================

`Club Laguna <https://www.instagram.com/club.laguna.cl>`__,
`Baldabiou <https://www.facebook.com/Baldabiou>`__

in the team for planning, setup, mixing

2022-05-02, |rz|



`Kosmoskonzerte #50 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte50-mit-alex-stolze-brueder-selke/>`__
=============================================================================================================================================

`Alex Stolze <http://alexstolze.com>`__,
|bs|

in the team for planning, setup, mixing

2022-04-11, |rz|



`Kosmoskonzerte #49 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-49/>`__
================================================================================================================

`Polina Borissova <https://www.facebook.com/polina.borissova>`__,
`Coucou <https://coucou.bandcamp.com>`__

in the team for planning, setup, mixing

2022-03-07, |rz|



`Kosmoskonzerte #48 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-48-simon-ansing-brueder-selke/>`__
===========================================================================================================================================

`Simon Ansing <https://soundcloud.com/simonansing>`__,
|bs|

in the team for planning, setup, mixing

2021-01-10, |rz|



`Kosmoskonzerte #47 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-47-christin-nichols-und-band/>`__
==========================================================================================================================================

Pfadfinder,
`Christin Nichols and band <https://www.facebook.com/Christin.Nichols.Official/>`__

in the team for planning, setup, mixing

2021-11-01, |rz|



`protest for preservation of the Rechenzentrum Potsdam <https://rz-potsdam.de/blog/15-9-demo>`__
================================================================================================

speech,
songwriter `Christian Näthe <http://christiannaethe.de>`__,
DJs Lukas Sweetwood & Kollektiv Compound,
band `Planet Obsolescence <https://planetobsolescence.bandcamp.com>`__

planning, setup, mixing of rolling sound reinforcement

2021-09-15



`Kosmoskonzerte #44 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-44-mit-andys-echo-luny/>`__
====================================================================================================================================

`Lunya <https://www.facebook.com/Lunya-208703526706157>`__,
`Andy’s Echo <https://andysecho.wordpress.com>`__

in the team for planning, setup, mixing

2021-09-06, |rz|



`Kosmoskonzerte #43 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-43-mit-inga/>`__
=========================================================================================================================

`INGA <https://trikont.de/category/artists/inga/>`__

in the team for planning, setup, mixing

2021-08-02, |rz|



`Kosmoskonzerte #42 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-42-manos-milonakis-brueder-selke/>`__
==============================================================================================================================================

`Manos Milonakis <http://www.manosmilonakismusic.com>`__ and
|bs|

in the team for planning, setup, mixing

2021-07-05, |rz|



`Kosmoskonzerte #41 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-41-mit-angela-aux/>`__
===============================================================================================================================

`Angela Aux <https://www.angela-aux.com>`__

in the team for planning, setup, mixing

2021-06-07, |rz|



`Kosmoskonzerte #40 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-40-mit-hasenscheisse/>`__
==================================================================================================================================

`Hasenscheisse <https://www.hasenscheisse.com>`__ (Duo)

planning, setup, mixing

2021-05-16, stream for `Sofa ohne Grenzen
<https://sofaohnegrenzen.de>`__ (live online charity festival) from |rz|

2021-05-24, `re-release for Kosmoskonzerte
<https://www.youtube.com/watch?v=IZM5d3ESld0>`__



`Kosmoskonzerte #39 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-39-mit-marta-cascales-alimbau-und-ceeys/>`__
=====================================================================================================================================================

`Marta Cascales Alimbau <http://www.martacascales.com>`__ and
|bs|

in the team for planning, setup, mixing

2021-04-19,
`stream <https://www.youtube.com/watch?v=JcO2PXpGPWI>`__
from |rz|



`Kosmoskonzerte #37 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-37-mit-sidney-busby/>`__
=================================================================================================================================

`Sydney Busby <https://sidneybusby.jimdofree.com>`__

planning, setup, mixing

2021-02-15,
`stream <https://www.youtube.com/watch?v=yCwhWw5EhII>`__
from |rz|



`Kosmoskonzerte #35 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-35-mit-luianna/>`__
============================================================================================================================

`Luianna <https://www.facebook.com/JasmineLuianna>`__ and band

in the team for booking, planning, setup, mixing

2020-11-02,
`stream <https://www.youtube.com/watch?v=D57izoaSHow>`__
from |rz|



`Kosmoskonzerte #32 <https://rz-potsdam.de/termin/kosmoskonzerte-32>`__
=======================================================================

Andi Haberl (Solo)

planning, setup, mixing

2020-08-03, |rz|



`Kosmoskonzerte #31 <https://rz-potsdam.de/termin/kosmoskonzerte-31>`__
=======================================================================

|ceeys|

in the team for planning, setup, mixing

2020-07-06,
`stream <https://www.youtube.com/watch?v=x5cA_rv4wzU>`__
from |rz|



`Kosmoskonzerte #30 <https://rz-potsdam.de/termin/kosmoskonzerte-30>`__
=======================================================================

`Mir Express <https://www.facebook.com/mirexpress/>`__

in the team for planning, setup, mixing

2020-06-08,
`stream <https://www.youtube.com/watch?v=6Rn39YNXE_w>`__
from |rz|



`Kosmoskonzerte #29 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-29-im-livestream-mit-dorothy-bird/>`__
===============================================================================================================================================

`Dorothy Bird <https://dorothybird.de>`__

in the team for planning, setup, mixing

2020-05-11,
`stream <https://www.youtube.com/watch?v=h2McoW7iv-U>`__
from |rz|



`Kosmoskonzerte #27 <https://rz-potsdam.de/termin/kosmoskonzerte-27>`__
=======================================================================

`Baldabiou <https://baldabiou.bandcamp.com>`__,
`Guiliana <https://www.facebook.com/music.giuliana>`__

in the team for booking, planning, setup, mixing

2020-02-03, |rz|



`Antikaroshi <http://theantikaroshi.de>`__ and Dr. Fleischmann
===============================================================

planning, in the team for setup and mixing

2020-02-01, |kuze|



`Kosmoskonzerte #26 <https://rz-potsdam.de/termin/kosmoskonzerte-26>`__
=======================================================================

`Theo Alexander <http://www.theo-alexander.com>`__,
`Hoshiko Yamane <https://www.hoshikoyamane.com>`__,
|ceeys|

in the team for planning, setup, mixing

2020-01-06, |rz|



`Kosmoskonzerte #24 <https://rz-potsdam.de/termin/kosmoskonzerte-24>`__
=======================================================================

`ZUSTRA <https://www.facebook.com/ZUSTRA.music/>`__,
`Alex St. Joan <https://alexstjoan.bandcamp.com>`__

in the team for planning, setup, mixing

2019-11-04, |rz|



`Kosmoskonzerte #23 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte23-mit-johannes-malfatti-midori-hirano-ceeys/>`__
=========================================================================================================================================================

`Johannes Malfatti <https://www.johannesmalfatti.com>`__,
`Midori Hirano <https://midorihirano.com>`__,
|ceeys|

in the team for planning, setup, mixing

2019-10-07, |rz|



15. historisches Apfelfest im Volkspark Potsdam
===============================================

`Cosmonautix <https://cosmonautix.com>`__, various performances

mixing

2019-09-28, `Alexandrowka Potsdam <https://alexandrowka.de>`__



`Olicía <https://www.oliciamusic.com/music/>`__
===============================================

planning, setup, mixing

2019-09-22, |kuze|



`Die Liedermacher Liga <https://www.facebook.com/liedermacherliga>`__
=====================================================================

planning, setup, mixing

2019-09-21, |kuze|



`Kosmoskonzerte #22 <https://rz-potsdam.de/termin/kosmoskonzerte-10-2/>`__
==========================================================================

`JPTR7 <https://www.facebook.com/jptr7music/>`__

in the team for planning, setup, mixing

2019-09-01, |rz|



`Kosmoskonzerte #21 <https://rz-potsdam.de/termin/kosmoskonzerte-21>`__
=======================================================================

`Les Enfants Sauvages <https://www.lesenfantssauvages.de>`__,
`Rosa Hoelger <https://rosahoelger.de>`__

in the team for planning, setup, mixing

2019-08-05, |rz|



`Kosmoskonzerte #20 <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/kosmoskonzerte-20-mit-marie-awadis-tim-linghaus-und-ceeys-2/>`__
==========================================================================================================================================================

`Marie Awadis <https://www.marie-awadis.com>`__,
`Tim Linghaus <https://www.timlinghaus.com>`__,
|ceeys|

in the team for planning, setup, mixing

2019-07-08, |rz|



Fête de la Musique
==================

`Adam Wendler <https://www.adamwendlermusic.com>`__,
`ZUSTRA <https://www.facebook.com/ZUSTRA.music>`__,
`Charlie Grant <http://charliegrant.co.uk>`__,
Mehmet Nar,
`Mojo Island <https://www.facebook.com/MojoIsland/>`__

planning, setup, mixing

2019-06-21, |rz|



`Operation Zeit <https://operation-zeit.de>`__
===============================================

in the team for planning, setup

2019-06-08, |kuze|



`Terminator Knödel <https://terminatorknoedel.bandcamp.com>`__
================================================================

2019-05-11, `Lagari Berlin <https://www.lagari-berlin.de>`__



`Kosmoskonzerte #18 <https://rz-potsdam.de/termin/kosmoskonzerte-18>`__
=======================================================================

`Angela Aux <https://www.angela-aux.com>`__,

in the team for planning, setup, mixing

2019-05-05, |rz|



`Kosmoskonzerte #17 <https://rz-potsdam.de/termin/kosmoskonzerte-17>`__
=======================================================================

`Simeon Walker <https://www.simeonwalker.co.uk/>`__,
`Aidan Baker <https://aidanbaker.bandcamp.com>`__,
|ceeys|

in the team for planning, setup, mixing

2019-04-08, |rz|



`Kosmoskonzerte #16 <https://rz-potsdam.de/termin/kosmoskonzerte-16>`__
=======================================================================

`Eat Ghosts <https://eatghosts.com>`__,
`CHICKN <https://chickn.bandcamp.com>`__

in the team for planning, setup, mixing

2019-03-04, |rz|



`Kosmoskonzerte #15 <https://rz-potsdam.de/termin/kosmoskonzerte-15>`__
=======================================================================

`Mystery Art Orchestra <https://www.mysteryartorchestra.com>`__,
`Philipp Cayler <https://www.facebook.com/philipp.cayler>`__

in the team for planning, setup, mixing

2019-02-04, |rz|



`Kosmoskonzerte #14 <https://rz-potsdam.de/termin/kosmoskonzerte-14>`__
=======================================================================

`Maike Zazie <https://maikezazie.bandcamp.com>`__,
`Simon Goff <https://www.simongoff.com>`__,
|ceeys|

in the team for planning, setup, mixing

2019-01-07, |rz|



`Kosmoskonzerte #13 <https://rz-potsdam.de/termin/kosmoskonzerte-13>`__
=======================================================================

`Telesonic 9000 <https://www.telesonic9000.com>`__,
`Alright Gandhi <https://alrightgandhi.bandcamp.com>`__

in the team for planning, setup, mixing, lighting

2018-12-05, `T-Werk Potsdam <https://www.t-werk.de>`__



`Kosmoskonzerte #12 <https://rz-potsdam.de/termin/kosmoskonzerte-12>`__
=======================================================================

`e.no <https://enoversum.bandcamp.com>`__,
`The North Chapter <https://www.facebook.com/thenorthchapter/>`__

in the team for planning, setup, mixing

2018-11-07, |kuze|



`Kosmoskonzerte #11 <https://rz-potsdam.de/termin/kosmoskonzerte-11>`__
=======================================================================

`Iván Muela <https://www.ivanmuela.com>`__,
`Marta Cascales Alimbau <https://www.martacascales.com>`__,
`Anne Müller <https://annemuller.bandcamp.com>`__,
|ceeys|

in the team for planning, setup, mixing

2018-10-08, |haus2|, |fL|



`Kosmoskonzerte #10 <https://rz-potsdam.de/termin/kosmoskonzerte-10>`__
=======================================================================

`Wooden Peak <https://woodenpeak.de>`__,
DJ

in the team for planning, setup, mixing

2018-09-15, |rz|



`Hi 5! <https://web.archive.org/web/20221001/https://rz-potsdam.de/cms/event/3geburtstag/>`__
=============================================================================================

political talk,
theater performance by `ArtChurch.Me <http://artchurch.me/>`__,
concert by `Soul Dressing <https://www.facebook.com/souldressingband/>`__

planning, setup, mixing

2018-09-01, |rz|



`Kosmoskonzerte #9 <https://rz-potsdam.de/termin/kosmoskonzerte-09>`__
======================================================================

`Ed Carlsen <http://edcarlsen.com>`__,
`Kat Galie <https://www.katgalie.com>`__,
`Grey Paris <https://greyparis.eu/>`__,
|ceeys|

in the team for planning, setup, mixing

2018-07-09, |rz|



`Die Liedermacher Liga <https://www.facebook.com/liedermacherliga>`__
=====================================================================

planning, setup, mixing

2018-06-16, |kuze|



`Kosmoskonzerte #8 <https://rz-potsdam.de/termin/kosmoskonzerte-8>`__
=====================================================================

.. currently unavailable (22-01-11):

  `Lennart Schilgen <https://www.lennartschilgen.de>`__,

Lennart Schilgen,
`Melvin Haack <https://business.facebook.com/inmelvinoveritas/>`__

in the team for planning, setup, mixing

2018-06-04, |rz|



`Kosmoskonzerte #7 <https://rz-potsdam.de/termin/2344/>`__
==========================================================

`Lumat Trio <https://www.facebook.com/lumat.music>`__,

in the team for planning, setup, mixing

2018-05-06, |rz|



`#netscape <https://uniater.de/produktionen/projekt-netzwerke/netscape/>`__
===========================================================================

theater piece

in the team for planning, setup, mixing

April 2018, Tiflis, Georgia



`Kosmoskonzerte #5 <https://rz-potsdam.de/termin/kosmoskonzerte-5>`__
=====================================================================

`Angela Aux <https://www.angela-aux.com>`__,

in the team for planning, setup, mixing

2018-03-07, |rz|



open stage
==========

planning, setup, mixing

2018-01-31, `HPI Potsdam <https://hpi.de>`__



`IM MODUS <https://www.wirsindimmodus.de/live/>`__
==================================================

planning, setup, mixing

2018-01-20, |rz|



`Kosmoskonzerte #4 <https://rz-potsdam.de/termin/kosmoskonzerte-4>`__
=====================================================================

`Garreth Broke <https://garrethbrooke.com>`__,
`Hania Rani <http://haniarani.com>`__,
`Joanna Gemma Auguri <https://joannagemmaauguri.com>`__,
|ceeys|

in the team for planning, setup, mixing

2018-01-08, |rz|



`Kosmoskonzerte #3 <https://rz-potsdam.de/termin/kosmoskonzerte-3>`__
=====================================================================

`Lane Of Lion <http://www.laneoflion.com>`__,
`Tim Anders <https://www.facebook.com/TimAndersOfficial/>`__,
`Fabio Shanti <https://www.facebook.com/FabioShanti/>`__

in the team for planning, setup, mixing

2017-12-04, |rz|



`Kosmoskonzerte #2 <https://rz-potsdam.de/termin/kosmoskonzerte-2>`__
======================================================================

`Martha Rose <http://martharosemusic.com>`__,
`Hesse <https://hesseh.bandcamp.com>`__

in the team for planning, setup, mixing

2017-11-06, |rz|



`Ma’at <https://uniater.de/en/productions/uniater-meets-artbridge/>`__
======================================================================

theater piece

in the team for planning, setup

2017-10-28, `University of Potsdam <https://uni-potsdam.de>`__



`Don't Cry, Work! <https://www.fritzahoi.de/produktionen/dont-cry-work/>`__
===========================================================================

theater piece

in the team for planning, setup, mixing

2017-10-13 & 2017-10-14,
`Spartacus Potsdam <https://www.spartacus-potsdam.de>`__



`Kosmoskonzerte #1 <https://rz-potsdam.de/termin/kosmoskonzerte-1>`__
=====================================================================

`Enzo Caterino <https://www.facebook.com/enzocaterino.cello/>`__,
`Kelly Wyse <https://www.kellywyse.com>`__,
`Lisa Morgenstern <http://www.lisa-morgenstern.com>`__,
|ceeys|

in the team for planning, setup, mixing

2017-10-09, |rz|



`Lucifer the Lightbearer <https://luciferthelightbearerhc.bandcamp.com>`__ and `Hedger <https://hedger.bandcamp.com>`__
=======================================================================================================================

planning, setup, mixing

2017-06-03, |kuze|



`Berlinska Dróha <http://www.berlinskadroha.com>`__
===================================================

in the team for planning, setup, mixing

2015-03-27, |kuze|
