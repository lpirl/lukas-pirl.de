===============================
DIY flexible acoustic treatment
===============================

:breadcrumb:
  {filename}/audio/index.rst audio

:description:
  tutorial on a do-it-yourself way of flexibly installing acoustic
  treatment



This is actually nothing special - I'll just present a set of pictures to show how to flexibly mount damping material.

This way, the damping material keeps easily exchangeable and can be mounted to another spot (e.g. if the furniture is being rearranged).

First, get some adequate acoustic panels, depending on the monetary resources and the frequencies to damp.
Cut them into a reasonable sized tiles.
Don't go for too large tiles - they maybe fold under their own weight.
The ones one the pictures below are 50 cm x 50 cm.


.. container:: m-col-t-12

  .. figure:: {attach}damping_mounting.jpeg
    :target: {attach}damping_mounting.jpeg
    :alt: acoustic foam mats along from the ceiling, close to the wall
    :width: 100%

    Battens mounted to the ceiling will carry all the tiles.
    Knock in nails in a distance according to the tile width.
    A small distance to the wall makes it look a little bit less gross.


.. container:: m-col-t-12

  .. figure:: {attach}damping_peg.jpeg
    :target: {attach}damping_peg.jpeg
    :alt: acoustic foam mat attached to a string with a pegs
    :width: 100%

    Now knot a peg to a piece of fishing line per tile.
    I recommend a bowline knot on each side (one going somewhere through the peg, the other one to be hung to the nail).
    Of course, the length of the fishing lines should match the final position of the tiles.


.. container:: m-col-t-12

  .. figure:: {attach}damping_mats.jpeg
    :target: {attach}damping_mats.jpeg
    :alt: acoustic foam mats along the wall, hanging
    :width: 100%

    An this is how it looks like.
    The irregular rotation is in no way disadvantageous for our intention (rather beneficial, if anything).


.. container:: m-col-t-12

  .. figure:: {attach}damping_dr.jpeg
    :target: {attach}damping_dr.jpeg
    :alt: acoustic foam mats on the ceiling over a drum set
    :width: 100%

    For very loud spots, it is also possible to mount some tiles to the ceiling.


.. container:: m-col-t-12

  .. figure:: {attach}damiping_ceiling_detail.jpeg
    :target: {attach}damiping_ceiling_detail. jpeg
    :alt: string holding acoustic foam mats on the ceiling
    :width: 100%

    While this is not exactly as flexible as the wall mounting, there is at least no peg required!
    Just again mount some battens with nails and strain some fishing lines close to the ceiling where you can put some tiles upon.


Enjoy!
