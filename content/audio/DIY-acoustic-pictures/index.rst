====================
DIY acoustic picture
====================

:breadcrumb:
  {filename}/audio/index.rst audio

:description:
  tutorial on how to build do-it-yourself acoustic pictures for damping



.. contents:: Contents:
  :depth: 1
  :class: m-col-m-4 m-right-m m-box m-frame

.. container:: m-col-m-8

  This article might be relevant to you, if the following points apply:

  * you want to get rid of too much acoustical reflections in a room
  * you do not like the aesthetics of plain acoustic foam
  * you do not want to spend large amounts of money for printed acoustic panels
    (aka *acoustic images* or *acoustic pictures*)

.. container:: m-row

  .. avoid content floating into each other

The results can look like this:

.. figure:: {attach}acoustic_pictures_mounted_a.jpg
  :target: {attach}acoustic_pictures_mounted_a.jpg
  :alt: mounted acoustic pictures
  :width: 100%

Dimensioning
============

I will not provide exact numbers in this tutorial since the acoustical requirements differ a lot from room to room.

However, as an example, I built the following set of pictures to enhance the sound of a room of the size 4 m x 4,6 m x 2,8 m:

* 1x 100 cm x 100 cm x 4 cm
* 2x 50 cm x 50 cm x 4 cm
* 6x 30 cm x 30 cm x 4 cm

(L x W x H)

When determining the sizes and the amount of the pictures to build, keep the following things in mind:

* the bigger the picture, the more it will absorb
* the bigger the picture, the harder to build

  * personally, I wouldn't exceed 1 m²

* the bigger the picture, the lower the ratio cost/damping

Placement
=========

When planning the positioning of your acoustic pictures, prefer the following spots:

* smooth/blank surfaces
* big surfaces
* at least one of two facing plain walls (**!**)

  * if there is no carpet, this also applies to the floor and the ceiling

* corners

  * also between walls and the ceiling

* near sound sources

  * speakers, instruments, …

Materials
=========

Insulation
----------

To choose the adequate sound insulation might be the toughest task.
Decide whether you want to grab some mineral wool from the hardware store
or if you want to use acoustic foam that is light, easily processible and
the best material for the job.

For best damping,
it is advisable to use the thickest insulation that fits into the frames you choose.
Obviously, you will need enough insulation to fill the inner bounds of every frame.
Don't just add the areas you will need -
take a sheet of paper and draw how you are going to cut the insulation later on,
since not having any loss is almost impossible due to geometric constraints.

When you are going to use **acoustic foam**,
just check out what fits best your frequency damping requirements and your budget.

If you choose to use **mineral wool** (think twice and don't),
you should make sure to close the back of the pictures you build as well,
You definitely don't want the itchy stuff to fly around in your living room.
This step won't be included in this tutorial (as I haven't tried it).

Frames
------

.. container:: m-row


  .. container:: m-col-m-6 m-nopadx

    For the frames, we will use stretcher frames. It's up to you if you
    buy the frames ready-made, the ledgers separately and assemble them
    yourself (figure 1) or - for virtuous wood processors - build them
    yourself from scratch. However, **it is important that they are
    tall enough to hold the insulation**.

  .. container:: m-col-m-6 m-nopad

    .. figure:: {attach}acoustic_pictures_1.jpg
      :target: {attach}acoustic_pictures_1.jpg
      :alt: ledgers to build a stretcher frame
      :width: 100%

      Figure 1: Ledgers to build a stretcher frame.


Cover
-----

Basically, you can use every fabric that is permeable for sound waves.
So: the thinner, the better but make sure it is thick enough to hide the insulation.
A good choice might be a thin, opaque molleton.

Don't forget that the covers must reach the back of the ledgers,
so for every dimension of the fabric, the following amount of fabric will be required at minimum:

.. _fabric_dimensions:

``<overall length of edge> + 2 * <tallness of ledger> + 2 * <width of ledger>``.

Again: draft your cut-outs to make sure you do not end up with too few fabric.

Toppings
--------

Feel free to add a further cover for the back side (fabric or thin compressed wood),
sewing work for the front cover or spacers between the wall and the picture for a lighter look.

Tools
-----

* hammer
* nails
* box cutter
* (tape/folding/…) rule
* electric iron
* staple gun
* for pictures 1 m² and above: fishing line

Putting it all together
=======================

Frames
------

.. container:: m-row

  .. container:: m-col-m-7 m-nopadx

    I used a plastic hammer to avoid damaging the wood (figure 2).
    Alternatively, you can put some wood between a regular hammer and
    the frames.

    For well-manufactured ledgers, you won't need any glue, screws or
    nails.

    Make sure the frames end up rectangular.

  .. container:: m-col-m-5 m-nopad

    .. figure:: {attach}acoustic_pictures_2_1.jpg
      :target: {attach}acoustic_pictures_2_1.jpg
      :alt: use a plastic hammer to avoid damaging the wood
      :width: 100%

      figure 2: use a plastic hammer to avoid damaging the wood



.. container:: m-row

  .. container:: m-col-m-7 m-nopadx

    For **large frames** (> 1 m²) or not very stiff insulation,
    it is advisable to add some nails to the frame so that the
    insulation won't slip out to the front (figure 3).

  .. container:: m-col-m-5 m-nopad

    .. figure:: {attach}acoustic_pictures_nails.jpg
      :target: {attach}acoustic_pictures_nails.jpg
      :alt: nails in a stretcher frame to tie fishing line to
      :width: 100%

      figure 3: use nails in the stretcher frame in order to keep the
      insulation in its place


.. container:: m-row

  .. container:: m-col-m-7 m-nopadx

    Additionally, some fishing line between the nails
    can avoid that the insulation tilts or bends
    against the back side of the front cover (figure 4).

  .. container:: m-col-m-5 m-nopad

    .. figure:: {attach}acoustic_pictures_fishing_line.jpg
      :target: {attach}acoustic_pictures_fishing_line.jpg
      :alt: assembled stretcher frame with proposed
      :width: 100%

      Figure 4: Add some fishing line (here: red) to prevent the
      insulation from tilting or bending against the front cover.

Cut the insulation
------------------

.. container:: m-row

  .. container:: m-col-m-7 m-nopadx

    For stiff materials (such as acoustic foam), cut the pieces a
    little bit larger, so that they jam themselves into the frame once
    they got squeezed into it.

  .. container:: m-col-m-5 m-nopad

    .. figure:: {attach}acoustic_pictures_cut.jpg
      :target: {attach}acoustic_pictures_cut.jpg
      :alt: assembled stretcher frame with proposed
      :width: 100%



Cover your frames
-----------------

.. container:: m-col-m-5 m-right-m m-nopad

  .. figure:: {attach}cover_the_frame.png
    :target: {attach}cover_the_frame.png
    :alt: order of stapling
    :width: 100%

    Figure 5: Staple towards the corners to get an even cover.

Cut your fabric according to your frames (remember the formula in
section 'Cover_') and iron it.

#. Spread the fabric front-side down and uncurly on some surface
   that is large enough
#. Put the frame on top (front-side down)
#. Center the frame on the fabric (vertically and horizontally)
#. Staple the fabric to the frame according to figure 5

   * Steps 1.x: pull but not too hard, otherwise you may warp your
     frame
   * Steps 2.x and 3.x: pull diagonally away from the center to keep
     the cover even

     * start in the middle of a ledger and staple towards the corner
     * do not staple the corners yet

#. Twofold the fabric at the corners and staple it to the frame
#. Do not tension the cover with the splines (if you have such)
   until you really need to (read: when your cover gets wavy)

Hang it!
--------

Et voila: put the insulation into the frame, hang it to the wall
and enjoy the new acoustic experience! :)

.. image:: {attach}acoustic_pictures_mounted_b_1.jpg
  :target: {attach}acoustic_pictures_mounted_b_1.jpg
  :width: 100%
  :alt: mounted acoustic pictures
