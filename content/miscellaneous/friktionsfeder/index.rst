==================================================================
Anfertigung einer Friktionsfeder für Fichtel & Sachs Getriebenaben
==================================================================

:breadcrumb:
  {filename}/miscellaneous/index.rst miscellaneous

:lang: de

:description:
  Anleitung zur Anfertigung einer Friktionsfeder für Fichtel & Sachs
  Getriebenaben



.. block-dim:: to non-German-speaking visitors

  Since products of Fichtel & Sachs were mainly distributed in
  German-speaking countries, this article is available in German only.

  However, if you are interested in this article translated to English
  or if you have any questions, please do not hesitate to `contact
  <{filename}/contact/index.rst>`_ me.

In dieser Anleitung beschreibe ich, wie du eine Friktionsfeder für
**Fichtel & Sachs** Getriebenaben herstellen kannst.

Im Speziellen geht es um die **Friktionsfeder 0113 100 000** für den Sperrklinkenträger einer Duomatic 102.
Du kannst die Feder auf den Bildern unten oder auf dieser `Teileliste
<http://scheunenfun.de/images/naben_infomaterial/pdf/duomatic-102-liste-1968.pdf>`_
identifizieren.

.. figure:: {attach}fs_dumatic102_friktionsfeder_1_markup_1.jpg
  :target: {attach}fs_dumatic102_friktionsfeder_1_markup_1.jpg
  :width: 100%
  :alt: Friktionsfeder eingebaut

.. figure:: {attach}fs_dumatic102_friktionsfeder_3_markup.jpg
  :target: {attach}fs_dumatic102_friktionsfeder_3_markup.jpg
  :alt: Friktionsfeder eingebaut, ohne Bremskonus
  :width: 100%

Solltest du eine **andere Feder** herstellen wollen, so musst du
selbstverständlich ggf. andere Materialien oder sogar andere Werkzeuge
wählen.


Materialien
===========

Ich habe mit folgenden Werkzeugen gute Erfahrungen gemacht.
Natürlich kannst du hier variieren, sollte dir etwas geeigneter
vorkommen oder falls du ein bestimmtes Werkzeug nicht zur Hand hast.

* Kombizange
* Wasserpumpenzange
* Seitenschneider
* Stecknuss für 11 mm (M7)

Und als Werkstoff:

* Federstahldraht, rund, ø1.2 mm, 10 cm pro Feder

.. figure:: {attach}fs_dumatic102_friktionsfeder_4_1.jpg
  :target: {attach}fs_dumatic102_friktionsfeder_4_1.jpg
  :alt: verwendet Materialien und verwendetes Werkzeug
  :width: 100%

Vorgehen
========

Wir werden den Stahl kalt verarbeiten, um seine Eigenschaften so wenig
wie möglich negativ zu beeinflussen.
Außerdem empfiehlt es sich, den Draht nur ein Mal zu biegen. Je stärker
er gebogen wird, desto mehr verliert er seine Federeigenschaften und
kann schnell brechen.

**Achtung**: Beim Schneiden springt der Draht gerne von der Zange weg.
Am besten du hältst ihn fest oder zielst in eine sichere Richtung.
Also: auf die Augen aufpassen.
Es kann auch mit Handschuhen gearbeitet werden: wenn der Draht beim
Biegen versehentlich umher schnippst, kann das auch etwas an den Händen
weh tun.

.. container:: m-col-t-12

  Zuerst klemmen wir den Draht mit der Wasserpumpenzange an der
  Stecknuss fest. Dabei sollte das kurze Ende des Drahtes ein klein
  wenig an der Zange überstehen.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_5.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_5.jpg
    :alt: Federstahldraht mit Wasserpumpenzange an Stecknuss
      festgehalten (Seite mit überstehendem Draht)
    :width: 100%

  .. figure:: {attach}fs_dumatic102_friktionsfeder_6.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_6.jpg
    :width: 100%
    :alt: Federstahldraht mit Wasserpumpenzange an Stecknuss
      festgehalten (Seite ohne überstehenden Draht)


.. container:: m-col-t-12

  Nun wickeln wir den Draht ca. 1¼ mal um die Stecknuss herum.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_7.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_7.jpg
    :width: 100%
    :alt: Federstahldraht um Stecknuss biegen

  .. figure:: {attach}fs_dumatic102_friktionsfeder_8.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_8.jpg
    :alt: Federstahldraht halb um Stecknuss gebogen
    :width: 100%

  .. figure:: {attach}fs_dumatic102_friktionsfeder_9.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_9.jpg
    :alt: Federstahldraht voll um Stecknuss gebogen
    :width: 100%


.. container:: m-col-t-12

  Danach sollte unser Werkstück so (oder so ähnlich) aussehen:

  .. figure:: {attach}fs_dumatic102_friktionsfeder_10.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_10.jpg
    :width: 100%
    :alt: Federstahldraht, einseitig zum Kreis gebogen


.. container:: m-col-t-12

  Nun machen wir uns daran, das ungebogene Ende des Drahtes 90° vom
  Ring weg zu biegen. Hierzu nehmen wir das Ende des gebogenen Teils
  des Drahtes, direkt vor dem ungebogenen Teil des Drahtes, in die
  Kombizange und biegen den Draht so, dass er mit 90° die Zange
  verlässt. Es geht besser, den Draht statt mit den Fingern mit einer
  harten Unterlage zu biegen.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_11.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_11.jpg
    :width: 100%
    :alt: Federstahldraht mit Kombizange fixiert um überstehendes Ende
      radial zum Kreis zu biegen

  .. figure:: {attach}fs_dumatic102_friktionsfeder_12.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_12.jpg
    :alt: Federstahldraht mit Kombizange fixiert, überstehendes Ende
      radial zum Kreis biegen
    :width: 100%


.. container:: m-col-t-12

  Das Werkstück sollte nun dem auf dem Foto ähneln:

  .. figure:: {attach}fs_dumatic102_friktionsfeder_13.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_13.jpg
    :alt: Federstahldraht mit Kombizange fixiert, überstehendes Ende
      radial zum Kreis gebogen
    :width: 100%


.. container:: m-col-t-12

  Als nächstes prüfen wir den Umfang der Feder. Ein geeigneter Radius
  ist, wenn ihr Außenradius zirka dem Außenradius ihres Trägers
  entspricht.

  Meine Federn waren nach dem Biegen immer ein klein wenig zu groß und
  ich habe sie mit vorsichtigem Zusammendrücken mit der
  Wasserpumpenzange aus zwei bis drei Richtungen etwas kleiner gemacht.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_14.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_14.jpg
    :alt: Kreis aus Federstahldraht mit Größe des Sperrklinkenträgers
      vergleichen
    :width: 100%

  .. figure:: {attach}fs_dumatic102_friktionsfeder_15.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_15.jpg
    :alt: Kreis aus Federstahldraht an Größe des Sperrklinkenträgers
      anpassen
    :width: 100%


.. container:: m-col-t-12

  Die Feder sollte nun schon gut auf dem Sperrklinkenträger passen:

  .. figure:: {attach}fs_dumatic102_friktionsfeder_16.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_16.jpg
    :alt: Kreis aus Federstahldraht an Größe des Sperrklinkenträgers
      angepasst
    :width: 100%


.. container:: m-col-t-12

  Um das auch direkt auszuprobieren, schneiden wir den überschüssigen
  Teil des Ringes ab.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_17.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_17.jpg
    :alt: Kreis aus Federstahldraht an Größe des Sperrklinkenträgers
      angepasst, Überschuss entfernt
    :width: 100%


.. container:: m-col-t-12

  Im Gegensatz zu heute üblichen Friktionsfedern, muss die Nase unserer
  Feder nun aber noch zusätzlich nach oben gebogen sein.
  Dazu nehmen wir den Draht komplett in die Wasserpumpenzange, so dass
  die bereits vorhandene 90° Biegestelle noch in der Zange ist und der
  ungebogene Teil des Drahtes gerade die Zange nach vorne verlässt.
  Auch hier biegen wir den Draht wieder 90° ab. Der ungebogene Teil des
  Drahtes sollte danach 5 mm neben dem Ring nach oben stehen.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_18.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_18.jpg
    :alt: Federstahldraht für Biegen axial zum Kreis in
      Wasserpumpenzange fixiert
    :width: 100%

  .. figure:: {attach}fs_dumatic102_friktionsfeder_19.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_19.jpg
    :alt: überstehenden Federstahldraht axial zum Kreis biegen
    :width: 100%


.. container:: m-col-t-12

  Wir erhalten folgendes Resultat:

  .. figure:: {attach}fs_dumatic102_friktionsfeder_20.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_20.jpg
    :alt: überstehenden Federstahldraht axial zum Kreis gebogen
    :width: 100%


.. container:: m-col-t-12

  Sehr gut. Der letzte Schritt ist ganz einfach: Wir schneiden den
  Draht so ab, dass das nach oben stehende Teil 10 mm lang ist:

  .. figure:: {attach}fs_dumatic102_friktionsfeder_21.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_21.jpg
    :alt: axial abstehender Federstahldraht gekürzt
    :width: 100%


.. container:: m-col-t-12

  Beim Zusammenbau müssen wir natürlich noch etwas aufpassen, dass die
  Friktionsfeder wirklich in der dafür vorgesehenen Bohrung endet,
  sonst verbiegt oder bricht sie bei der Jungfernfahrt.

  .. figure:: {attach}fs_dumatic102_friktionsfeder_22_markup.jpg
    :target: {attach}fs_dumatic102_friktionsfeder_22_markup.jpg
    :alt: Einbau des axial abstehenden Federstahldraht in Bremskonus
    :width: 100%

Solltest du Kritik oder Verbesserungsvorschläge haben, lass es mich
wissen.

Gute Fahrt!
