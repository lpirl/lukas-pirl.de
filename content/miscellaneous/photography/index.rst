Photography
===========

:breadcrumb:
  {filename}/miscellaneous/index.rst miscellaneous

:description:
  long-ago activities of Lukas Pirl related to photography



Long time ago, I was into photography, but that's history.

.. container:: m-row

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}branch.jpeg
      :target: {attach}branch.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}frost.jpeg
      :target: {attach}frost.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}kudu.jpeg
      :target: {attach}kudu.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}lightning.jpeg
      :target: {attach}lightning.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}lion_m.jpeg
      :target: {attach}lion_m.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}lions_f.jpeg
      :target: {attach}lions_f.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}matches.jpeg
      :target: {attach}matches.jpeg
      :width: 100%

  .. container:: m-col-t-6 m-col-s-3

    .. image:: {attach}reptile.jpeg
      :target: {attach}reptile.jpeg
      :width: 100%
