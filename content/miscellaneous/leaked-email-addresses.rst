======================
leaked email addresses
======================

:breadcrumb:
  {filename}/miscellaneous/index.rst miscellaneous

:description:
  list of parties that disclosed personal information (email, name)



When handing out an email address (websites, companies, clubs etc.),
I usually choose a unique email address each.
When receiving email spam, it is thus easy to tell which parties leak
(sell?) customers' email addresses.

Despite repeated requests, none of the parties investigated the case
credibly (they denied the fact or claimed they tried hard but didn't
find a cause) nor did any provide a list of third parties that
processed the data (pre-GDPR).

Spam email has been received for uniquely assignable email addresses
handed out to the following parties.

Personalized spam email (first and last name):

* `Stiftung United Internet for UNICEF
  <https://www.united-internet-for-unicef-stiftung.de>`__
  (2024)
* `Verlag Der Tagesspiegel GmbH <http://www.tagesspiegel.de/>`__
  (2015)
* `tonerpreis.de - MSK Bürotechnik Handels GmbH
  <https://www.tonerpreis.de/>`__
  (2017)
* `bohrerdiscount24.de - Rosenkranz / Spickhoff GbR
  <https://www.bohrerdiscount24.de/>`__
  (2015)
* `KOKA 36 Konzertkasse e. K. <http://www.koka36.de/>`__
  (2015)
* `hans-hats.de / home4u-shop.de - Temme GmbH
  <https://www.home4u-shop.de>`__
  (2015)
* `Fabial <http://www.fabial.de/>`__
  (2014)

Not personalized spam email:

* `kino-potsdam.de - Drünkler & Trauzettel GbR
  <http://www.kino-potsdam.de/>`__
  (2015)

Illegitimate use:

* `Flexera <https://www.flexera.com/>`__ used the contact details
  provided to RightScale after Flexera acquired RightScale (2019)
