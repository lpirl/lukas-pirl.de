=============
miscellaneous
=============

:html_header:
  <style>
  section:nth-last-child(-n+2) { color: var(--dim-color);  }
  section:nth-last-child(-n+2) > h2 { font-weight: inherit; }
  section:nth-last-child(-n+2) a { color: var(--dim-color); }
  </style>

:description:
  list of miscellaneous activities by Lukas Pirl



`OpenStreetMap contributions <https://www.openstreetmap.org/user/lukas_p/history>`__
====================================================================================



`Anfertigung einer Friktionsfeder für Fichtel & Sachs Getriebenaben <{filename}friktionsfeder/index.rst>`__
===========================================================================================================

.. text-dim::

  German language only. Approximate English title: "Manufacture a
  Friction Spring for Fichtel & Sachs Gear Hubs".



`leaked email addresses <{filename}leaked-email-addresses.rst>`__
=================================================================



`photography <{filename}photography/index.rst>`__
=================================================
