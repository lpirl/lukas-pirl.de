============
legal notice
============

:description:
  legal notice for web services offered by Lukas Pirl



Person who runs this website:

| Lukas Pirl
| 14482 Potsdam
| Germany

.. include:: contact/email.part.rst

license
=======

Unless state otherwise (e.g., where work of other parties is
referenced), the material (text, images, etc.) on this website is
licensed under a `Creative Commons Attribution-ShareAlike 4.0
International License
<http://creativecommons.org/licenses/by-sa/4.0/>`__.

disclaimer
==========

For the purposes of this Disclaimer, *Service* – no matter if singular
or plural – refers to this website and the functionality it offers.
*We* refers to the person who runs this website (see above).
*You* refers to the individual accessing the Service, or another legal
entity on behalf of which such individual is accessing or using the
Service, as applicable.

The information contained on the Service is for general information
purposes only.

We assume no responsibility for errors or omissions in the contents of the Service.

In no event shall We be liable for any special, direct, indirect, consequential, or incidental damages or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in connection with the use of the Service or the contents of the Service. We reserve the right to make additions, deletions, or modifications to the contents on the Service at any time without prior notice.

We do not warrant that the Service is free of viruses or other harmful
components.

external links
--------------

The Service may contain links to external websites that we do not
provide or maintain with which We are affiliated in any way.

Please note that We do not guarantee the accuracy, relevance,
timeliness, or completeness of any information or functionality on
these external websites.

errors and omissions
--------------------

The information given by the Service is for general guidance on matters
of interest only. Even if We take every precaution to insure that the
content of the Service is both current and accurate, errors can occur.
Plus, given the changing nature of laws, rules and regulations, there
may be delays, omissions or inaccuracies in the information contained
on or functionality provided by the Service.

We are not responsible for any errors or omissions, or for the results
obtained from the use of this Service' information or functionality.

fair use
--------

We may use copyrighted material which has not always been specifically
authorized by the copyright owner. We are making such material
available for criticism, comment, news reporting, teaching,
scholarship, or research.

We believe this constitutes a "fair use" of any such copyrighted
material as provided for in section 107 of the United States Copyright
law.

If You wish to use copyrighted material from the Service for your own
purposes that go beyond fair use, You must obtain permission from the
copyright owner.

no responsibility
-----------------

In no event shall We be liable for any special, incidental, indirect,
or consequential damages whatsoever arising out of or in connection
with your access or use or inability to access or use the Service.

use at your own risk
--------------------

All information and functionality in the Service are provided "as is",
with no guarantee of completeness, accuracy, timeliness or of the
results obtained from the use of this information or functionality, and
without warranty of any kind, express or implied, including, but not
limited to warranties of performance, merchantability and fitness for a
particular purpose.

We will not be liable to You or anyone else for any decision made or
action taken in reliance on the information given or functionality
provided by the Service or for any consequential, special or similar
damages, even if advised of the possibility of such damages.

----

.. text-dim::

  The disclaimer_ has been generated with
  `TermsFeed <https://www.termsfeed.com/>`__.
