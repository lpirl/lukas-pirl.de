=======
contact
=======

:html_header:
  <style>
  #social-media { color: var(--dim-color); }
  #social-media a { color: var(--dim-color); }
  </style>

:description:
  contact information of Lukas Pirl

email
=====

preferred

.. include:: email.part.rst


social media
============

not actively used

- `LinkedIn <http://de.linkedin.com/pub/lukas-pirl/6a/756/48/>`__
- `XING <https://www.xing.com/profile/Lukas_Pirl>`__
- `facebook <https://www.facebook.com/lukas.pirl>`__
- `Instagram <https://www.instagram.com/lukas_pirl/>`__
- `Twitter <https://twitter.com/lukas_pirl>`__
