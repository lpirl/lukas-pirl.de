.. keep in sync with ``impressum.rst``

| website@lukas-pirl.de
| GPG available (search, e.g., `the Ubuntu key server
  <https://keyserver.ubuntu.com/pks/lookup?search=lukas+pirl&fingerprint=on&op=index>`__)
