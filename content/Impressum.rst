=========
Impressum
=========

:lang: de

:description:
  Impressum für von Lukas Pirl angebotene Web-Dienste



Betreiber
=========

| Lukas Pirl
| 14482 Potsdam


Kontakt
=======

| website@lukas-pirl.de
| GPG verfügbar (z. B. auf dem `Schlüssel-Server von Ubuntu
  <https://keyserver.ubuntu.com/pks/lookup?search=lukas+pirl&fingerprint=on&op=index>`__)

Datenschutz
===========

Da die Inhalte dieser Webseite in englischer Sprache angeboten werden,
ist ebenfalls die `Datenschutzerklärung in englischer Sprache
<{filename}privacy-policy/gitlab.rst>`__ verfasst.

Urheberrecht
============

Die durch den Betreiber erstellten Inhalte und Werke auf diesen Seiten
unterliegen dem deutschen Urheberrecht. Soweit die Inhalte auf dieser
Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte
Dritter beachtet. Insbesondere werden Inhalte Dritter als solche
gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung
aufmerksam werden, bittet der Betreiber um einen entsprechenden
Hinweis. Bei Bekanntwerden von Rechtsverletzungen wird der Betreiber
derartige Inhalte umgehend entfernen.

Ausgenommen Inhalte und Werke Dritter, sind die Inhalte und Werke
dieser Webseite lizenziert unter einer `Creative Commons Namensnennung
und Weitergabe unter gleichen Bedingungen 4.0 International
<http://creativecommons.org/licenses/by-sa/4.0/>`__-Lizenz.

Haftung für Inhalte
===================

Als Diensteanbieter ist der Betreiber gemäß § 7 Abs.1 TMG für eigene
Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich.
Nach §§ 8 bis 10 TMG ist der Betreiber als Diensteanbieter jedoch nicht
verpflichtet, übermittelte oder gespeicherte fremde Informationen zu
überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige
Tätigkeit hinweisen.

Verpflichtungen zur Entfernung oder Sperrung der Nutzung von
Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt.
Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden
von entsprechenden Rechtsverletzungen wird der Betreiber diese Inhalte
umgehend entfernen.

Haftung für Links
=================

Dieses Angebot enthält Links zu externen Websites Dritter, auf deren
Inhalte der Betreiber keinen Einfluss hat. Deshalb kann der Betreiber
für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte
der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber
der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt
der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige
Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.

Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch
ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
Bekanntwerden von Rechtsverletzungen wird der Betreiber derartige Links
umgehend entfernen.

----

.. text-dim::

  Die Absätze bzgl. der Haftung wurden generiert mit
  `eRecht24 <https://www.e-recht24.de>`__
