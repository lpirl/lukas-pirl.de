.. image:: https://gitlab.com/lpirl/lukas-pirl.de/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/lukas-pirl.de/pipelines
  :align: right

This repository holds the sources of https://lukas-pirl.de.

The Web site has been created with the static site generator `Pelican
<https://getpelican.com>`__ and the wonderful theme (framework) `m.css
<https://mcss.mosra.cz/>`__ by `Vladimír Von­druš <https://mosra.cz>`__.

getting started
===============

Make sure you have the following pre-requisites installed:

* ``git``: version control system
* ``git-lfs``: for the BLOBs in the repository
* ``make``: to use the shortcuts in the ``Makefile``
* ``python-pip3``: Python package manager

.. * ``graphviz``: for generating graphs

And those are the first commands you probably need:

.. code:: shell

  git clone --recurse-submodules <url>

  cd lukas-pirl.de

  # if you wish, create & enter a virtual environment, e.g.:
  # ``python3 -m virtualenv venv``
  # ``. venv/bin/activate``

  # install dependencies:
  make setup

  # to generate the HTML output once:
  make html

  # to generate the HTML output on changes:
  make regenerate

  # to run a local development server on http://localhost:8000/
  # (regenerates HTML automatically):
  make devserver

CI
==

building
--------

The CI builds the Web pages (``make pages``). Whenever there are
warnings or errors building the Web pages, the pipeline will fail.

link checking
-------------

The CI checks links and generates `a linkchecker report upon commit to the
master branch <https://gitlab.com/lpirl/lukas-pirl.de/-/jobs/artifacts/master/file/linkchecker-out.html?job=test>`__.

See also `the GitLab CI configuration <.gitlab-ci.yml>`__ and
`the linkchecker configuration <.linkcheckerrc>`__.

frameworks' docs
================

* `m.css: theme (framework)
  <https://mcss.mosra.cz/themes/pelican/>`__
* `Pelican: site generator
  <https://docs.getpelican.com/en/stable/content.html>`__,
* `Sphinx (the framework Pelican uses)
  <https://www.sphinx-doc.org/en/master/>`__.
